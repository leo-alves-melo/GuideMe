//
//  Tour.swift
//  GuideMe
//
//  Created by Leonardo Alves de Melo on 21/06/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import Foundation

class Tour {
    
    var checkpointsList: [Checkpoint] = []
    var dificult: Int //De 0 a 10
    var didComplete: Bool
    var title: String
    var estimatedTime: Int
    var totalDistance: Int
    var attractionsList: [Attraction] = []
    
    init(checkpointsList: [Checkpoint], dificult: Int, didComplete: Bool, title: String, estimatedTime: Int, totalDistance: Int, attractionsList: [Attraction]) {
        self.checkpointsList = checkpointsList
        self.dificult = dificult
        self.didComplete = didComplete
        self.title = title
        self.estimatedTime = estimatedTime
        self.totalDistance = totalDistance
        self.attractionsList = attractionsList
    }
    
}
