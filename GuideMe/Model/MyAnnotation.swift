//
//  MyAnnotation.swift
//  GuideMe
//
//  Created by Leonardo Alves de Melo on 21/06/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import MapKit

class MyAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var ID: String
    
    init(coordinate: CLLocationCoordinate2D, ID: String) {
       
        
        self.coordinate = coordinate
        self.ID = ID
        
         super.init()
    }
}
