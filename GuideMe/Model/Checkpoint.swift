//
//  CheckPoint.swift
//  GuideMe
//
//  Created by Leonardo Alves de Melo on 21/06/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import MapKit

class Checkpoint {
    
    var location: CLLocation
    var distanceToNext: Int
    var checked = false
    
    init(location: CLLocation, distanceToNext: Int) {
        self.location = location
        self.distanceToNext = distanceToNext
    }
}
