//
//  Attraction.swift
//  GuideMe
//
//  Created by Leonardo Alves de Melo on 21/06/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import MapKit

class Attraction: Checkpoint {
    
    var information: String
    var image: UIImage
    var title: String
    
    init(information: String, location: CLLocation, distanceToNext: Int, image: UIImage, title: String) {
        self.information = information
        self.image = image
        self.title = title
        
        super.init(location: location, distanceToNext: distanceToNext)
    }
    
    
    init(information: String, checkpoint: Checkpoint, image: UIImage, title: String) {
        self.information = information
        self.image = image
        self.title = title
        
        super.init(location: checkpoint.location, distanceToNext: checkpoint.distanceToNext)
    }
}
