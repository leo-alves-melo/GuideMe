//
//  TourServices.swift
//  GuideMe
//
//  Created by Leonardo Alves de Melo on 22/06/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import MapKit

class TourServices {
    
    func getTours(completion: ([Tour])->Void) {
        
        //centro medico: -22.808687, -47.064849
        //centro: -22.9064, -47.0616
        //rotatoria:-22.814197, -47.058870
        //meio da rua na frente do eldorado sentido rs: -22.813651, -47.061079

        let ckp01 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 10.0), distanceToNext: 0)
        let ckp02 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 20.0), distanceToNext: 0)
        let ckp03 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 30.0), distanceToNext: 0)
        let ckp04 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 40.0), distanceToNext: 0)
        let ckp05 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 50.0), distanceToNext: 0)
        let ckp06 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 60.0), distanceToNext: 0)
        let ckp07 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 70.0), distanceToNext: 0)
        let ckp08 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 80.0), distanceToNext: 0)
        let ckp09 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 90.0), distanceToNext: 0)
        let ckp10 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 100.0), distanceToNext: 0)
        
        
        let atr01 = Attraction(information: "lalalalala", checkpoint: ckp04, image: #imageLiteral(resourceName: "seta"), title: "atração 1")
        let atr02 = Attraction(information: "lalalalala", checkpoint: ckp07, image: #imageLiteral(resourceName: "seta"), title: "atração 2")
        let atr03 = Attraction(information: "lalalalala", checkpoint: ckp10, image: #imageLiteral(resourceName: "seta"), title: "atração 3")
        
        let tour = Tour(checkpointsList: [ckp01, ckp02, ckp03, atr01, ckp05, ckp06, atr02, ckp08, ckp09, atr03],
                        dificult: 1, didComplete: false, title: "Testinho", estimatedTime: 12, totalDistance: 12, attractionsList: [atr01, atr02, atr03])
        
        let ckp11 = Checkpoint(location: CLLocation(latitude: 0.0, longitude: 10.0), distanceToNext: 0)
        let ckp12 = Checkpoint(location: CLLocation(latitude: -22.813651, longitude: -47.061079), distanceToNext: 0)
        
        let atr04 = Attraction(information: "lalalalala", checkpoint: ckp12, image: #imageLiteral(resourceName: "seta"), title: "atração 4")
        
        let tour2 = Tour(checkpointsList: [ckp11, ckp12], dificult: 0, didComplete: false, title: "s", estimatedTime: 0, totalDistance: 10, attractionsList: [atr04])
        
        completion([tour2, tour])
 
    }
}
