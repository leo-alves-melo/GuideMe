//
//  ViewController.swift
//  GuideMe
//
//  Created by Leonardo Alves de Melo on 19/06/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var viewAnnotationLabel: UILabel!
    @IBOutlet weak var getAngleLabel: UILabel!
    
    var contadorViewAnnotation = 0
    var contadorGetAngle = 0
    
    var firstZoom = true
    
    var lastAngle = 0.0
    
    var locationAngle = CLLocationDirection(exactly: 0)
    
    var locationManager: CLLocationManager!
    
    var selfLocation: CLLocation!
    var targetLocation: CLLocation!
    var targetAngle: Double = 0.0
    
    var arrowAnnotation = MKPointAnnotation()
    
    var manager: TourManager!
    
    
    var arrowView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        

        let tourServices = TourServices()
        tourServices.getTours { (tours) in
            self.manager = TourManager(tour: tours[0], delegate: self)
            self.manager.start()
        }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: -22.813651, longitude: -47.061079)
        annotation.title = "target"
        mapView.addAnnotation(annotation)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.selfLocation = locations.last!
        
        if self.manager != nil {
            self.manager.iterate(userLocation: self.selfLocation)
        }
        
        if firstZoom {
            self.zoomToLocation(location: self.selfLocation.coordinate)
            self.firstZoom = false
        }
        
        
    }
    
    func zoomToLocation(location: CLLocationCoordinate2D) {
        let viewRegion = MKCoordinateRegionMakeWithDistance(location, 400, 400)
        mapView.setRegion(viewRegion, animated: false)
    }

}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        self.contadorViewAnnotation += 1
        self.viewAnnotationLabel.text = "view annotation: \(contadorViewAnnotation)"
        
        
        
        if annotation.title == "target" {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "target")
            
            let targetView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            targetView.image = #imageLiteral(resourceName: "Oval")
            annotationView.addSubview(targetView)
            
            return annotationView
        } else {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "")
            
            arrowView.image = #imageLiteral(resourceName: "seta_cima")
            //self.arrowView.transform = self.arrowView.transform.rotated(by: .pi/2.0)
            annotationView.addSubview(arrowView)

            return annotationView
        }
       
    }
    
}

extension ViewController: TourManagerDelegate {
    
    func setDistance(distance: Int) {
        print("Distance: \(distance)")
        self.distanceLabel.text = "Distância: \(distance)m"
    }
    
    func setAngle(angle: Double) {
        print("Angle: \(angle)")
        self.arrowView.transform = CGAffineTransform.identity
        
        self.arrowView.transform = self.arrowView.transform.rotated(by: CGFloat(-angle ))
        
        self.contadorGetAngle += 1
        self.getAngleLabel.text = "get angle \(self.contadorGetAngle)"
    }
    
    func didArriveInAttraction(attraction: Attraction) {
        print("Chegou na Atração: \(attraction.title)")
    }
    
    func didArriveInLastAttraction(attraction: Attraction) {
        print("CABOU: \(attraction.title)")
        self.distanceLabel.text = "CABOU: \(attraction.title)"
    }
    
}
