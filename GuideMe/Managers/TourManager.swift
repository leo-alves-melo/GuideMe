//
//  TourManager.swift
//  GuideMe
//
//  Created by Leonardo Alves de Melo on 21/06/18.
//  Copyright © 2018 Leonardo Alves de Melo. All rights reserved.
//

import MapKit

protocol TourManagerDelegate : class {
    func setDistance(distance: Int)
    func setAngle(angle: Double)
    func didArriveInAttraction(attraction: Attraction)
    func didArriveInLastAttraction(attraction: Attraction)
}

class TourManager {
    
    private var tour: Tour
    private var nextCheckpoint: Int = 0
    private var distanceToNextAttraction = 0 //Distancia entre o proximo checkPointe a próxima atração
    private var nextAttraction: Int = 0
    private weak var delegate: TourManagerDelegate?
    var distaceToValidateCheck: Int = 10
    
    
    init(tour: Tour, delegate: TourManagerDelegate) {
        self.tour = tour
        self.delegate = delegate
    }
    
    func start() {
        tour.checkpointsList[self.nextCheckpoint].checked = true
        
        for checkpoint in self.tour.checkpointsList  {
            if checkpoint.checked == true {
                self.nextCheckpoint += 1
                
                if checkpoint is Attraction {
                    self.nextAttraction += 1
                }
            }
        }
        
    }
    
    func iterate(userLocation: CLLocation) {
        
        let distanceToNextCheckpoint = self.getDistanceToNextCheckpoint(userLocation: userLocation)
        
        //Se chegou em um checkpoint
        if distanceToNextCheckpoint < distaceToValidateCheck {
            
            self.tour.checkpointsList[self.nextCheckpoint].checked = true
            
            //Se o checkpoint chegado é uma atração
            if self.tour.checkpointsList[self.nextCheckpoint] is Attraction {
                
                //Se a atração que chegamos é a última
                if self.nextAttraction == self.tour.attractionsList.count - 1 {
                    self.delegate?.didArriveInLastAttraction(attraction: self.tour.attractionsList[self.nextAttraction])
                //Se não for a última
                } else {
                    self.delegate?.didArriveInAttraction(attraction: self.tour.attractionsList[self.nextAttraction])
                    self.tour.didComplete = true
                    self.nextAttraction += 1
                    
                }
            //Se o checkpoint não for uma atração
            }
            
            //TODO: descobrir se quanto for a ultima atração pode dar algum problema graças a essa linha abaixo
            self.nextCheckpoint += 1
            
            
        } else {
            self.delegate?.setDistance(distance: distanceToNextCheckpoint + self.distanceToNextAttraction)
            self.delegate?.setAngle(angle: self.getAngleToNextCheckpoint(userLocation: userLocation))
        }
    }
    
    func abort() {
        //mandar salvar o progresso
    }
    
    private func getDistanceToNextCheckpoint(userLocation: CLLocation) -> Int {
        return  Int(userLocation.distance(from: self.tour.checkpointsList[self.nextCheckpoint].location))
        
    }
    
    private func getAngleToNextCheckpoint(userLocation: CLLocation) -> Double {
        
        let nextCheckpointLocation = self.tour.checkpointsList[self.nextCheckpoint].location

        let subtractionLongitude = nextCheckpointLocation.coordinate.longitude - userLocation.coordinate.longitude
  
        
        let y = sin(subtractionLongitude) * cos(nextCheckpointLocation.coordinate.latitude)
        let x = cos(userLocation.coordinate.latitude) * sin(nextCheckpointLocation.coordinate.latitude) - sin(userLocation.coordinate.latitude) * cos(nextCheckpointLocation.coordinate.latitude) * cos(subtractionLongitude)
        let arcAngle = atan2(y, x)
        
        return arcAngle
    }
}
